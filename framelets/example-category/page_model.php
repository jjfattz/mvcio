<?php 

class model {
	private $page_title;
	
	public function __construct() {
		$this->page_title = 'Post Page';
	}
	
	public function get_title() {
		return $this->page_title;
	}
}
