<?php 

include('page_model.php');

class controller extends model {
	public function run() {
		$this->get_model();
		$this->get_view();
	}
	
	private function get_model() {
		require_once('page_model.php');
	}
	
	private function get_view() {
		require_once('page_view.php');
	}
}
