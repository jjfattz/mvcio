<?php 

include('category_model.php');

class controller extends model {
	public function run() {
		$this->get_model();
		$this->get_view();
	}
	
	private function get_model() {
		require_once('category_model.php');
	}
	
	private function get_view() {
		require_once('category_view.php');
	}
}
