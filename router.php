<?php 

class url_router {
	private $base_url;
	private $mvcio_dir;
	
	public function __construct($_base_url, $_mvcio_dir) {
		// initialize
		$this->base_url = $_base_url;
		$this->mvcio_dir = $_mvcio_dir;
	}
	
	public function route($uri) {
		// route url based on category in URI
		$req = $this->get_controller($uri);
		require_once($this->mvcio_dir.$req);
		$controller = new controller();
		$controller->run($this->strip_base_url($uri));
	}
	
	private function get_controller($uri) {
		if(substr($uri, -1) != '/') { // if url does not have trailing / redirect with /
			header("location: {$uri}/");
			exit();
		}
		
		$uri = $this->strip_base_url($uri);
		if(strlen($uri) == 0) { // if index
			return 'index/category_controller.php';
		}
		
		if(!file_exists($this->mvcio_dir.substr($uri, 0, strpos($uri, '/')))) { // if framelet doesn't exist, send to 404 controller
			return '404/category_controller.php';
		}
		
		$dir_count = substr_count($uri, '/');
		
		if($dir_count == 1) {
			return $uri.'/category_controller.php';
		} else {
			return substr($uri, 0, strpos($uri, '/')).'/page_controller.php';
		}
	}
	
	private function strip_base_url($uri) {
		return substr($uri, strpos($uri, $this->base_url)+strlen($this->base_url));
	}
}


// implementation
$router = new url_router($base_url, $mvcio_dir); // create url router object
echo $router->route($_SERVER['REQUEST_URI']); // route uri request to proper view


